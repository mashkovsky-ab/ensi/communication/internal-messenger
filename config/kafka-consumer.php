<?php

use Ensi\LaravelInitialEventPropagation\RdKafkaConsumerMiddleware;

return [
    'global_middleware' => [
        RdKafkaConsumerMiddleware::class,
    ],

    'processors' => [
        [
            /*
            | Optional, defaults to `null`.
            | Here you may specify which topic should be handled by this processor.
            | Processor handles all topics by default.
            */
            'topic' => topic('communication.test.example.1'),

            /*
            | Optional, defaults to `null`.
            | Here you may specify which ensi/laravel-phprdkafka consumer should be handled by this processor.
            | Processor handles all consumers by default.
            */
            'consumer' => 'default',

            /*
            | Optional, defaults to `action`.
            | Here you may specify processor's type. Defaults to `action`
            | Supported types:
            |  - `action` - a simple class with execute method;
            |  - `job` - Laravel Queue Job. It will be dispatched using `dispatch` or `dispatchSync` method;
            */
            'type' => 'action',

            /*
            | Required.
            | Fully qualified class name of a processor class.
            */
            // for async action test
            //'class' => \App\Domain\Example\Actions\AsyncHandleKafkaTopicAction::class,
            // for sync action test
            'class' => \App\Domain\Example\Actions\SyncHandleKafkaTopicAction::class,

            /*
            | Optional, defaults to `false`.
            | Proxy messages to Laravel's queue.
            | Supported values:
            |  - `false` - do not stream message. Execute processor in syncronous mode;
            |  - `true` - stream message to Laravel's default queue;
            |  - `<your-favorite-queue-name-as-string>` - stream message to this queue;
            */
            // for async action test
            //'queue' => true,
            // for sync action test
            'queue' => false,
        ],
        [
            'topic' => topic('communication.command.create-message.1'),
            'consumer' => 'default',
            'type' => 'action',
            'class' => \App\Domain\Messages\Actions\Kafka\SyncCreateMessageAction::class,
            'queue' => false,
        ],
    ],
    'consumer_options' => [
        'default' => [
            'consume_timeout' => 5000,
            'middleware' => [],
        ],
    ],
];
