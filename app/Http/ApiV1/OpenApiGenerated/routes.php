<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

use Illuminate\Support\Facades\Route;

Route::post('chats:search', [\App\Http\ApiV1\Modules\Messages\Controllers\ChatsController::class, 'search'])->name('searchChats');
Route::post('chats', [\App\Http\ApiV1\Modules\Messages\Controllers\ChatsController::class, 'create'])->name('createChat');
Route::delete('chats/{id}', [\App\Http\ApiV1\Modules\Messages\Controllers\ChatsController::class, 'delete'])->name('deleteChat');
Route::patch('chats/{id}', [\App\Http\ApiV1\Modules\Messages\Controllers\ChatsController::class, 'patch'])->name('patchChat');
Route::post('messages:search', [\App\Http\ApiV1\Modules\Messages\Controllers\MessagesController::class, 'search'])->name('searchMessages');
Route::post('attachments', [\App\Http\ApiV1\Modules\Messages\Controllers\AttachmentsController::class, 'upload'])->name('createAttachment');
Route::delete('attachments', [\App\Http\ApiV1\Modules\Messages\Controllers\AttachmentsController::class, 'delete'])->name('deleteAttachment');
