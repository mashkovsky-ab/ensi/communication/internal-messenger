<?php

namespace App\Http\ApiV1\Modules\Messages\Controllers;

use App\Http\ApiV1\Modules\Messages\Queries\MessagesQuery;
use App\Http\ApiV1\Modules\Messages\Resources\MessagesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;

class MessagesController
{
    public function search(PageBuilderFactory $pageBuilderFactory, MessagesQuery $query)
    {
        return MessagesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
