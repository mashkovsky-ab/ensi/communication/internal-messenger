<?php

namespace App\Http\ApiV1\Modules\Messages\Controllers;

use App\Domain\Messages\Actions\DeleteFileAction;
use App\Domain\Messages\Actions\UploadFileAction;
use App\Http\ApiV1\Modules\Messages\Requests\DeleteFileRequest;
use App\Http\ApiV1\Modules\Messages\Requests\UploadFileRequest;
use App\Http\ApiV1\Modules\Messages\Resources\AttachmentsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class AttachmentsController
{
    public function upload(UploadFileRequest $request, UploadFileAction $action)
    {
        $file = $request->file('file');
        $fileName = $request->get('name');

        return new AttachmentsResource($action->execute($file, $fileName));
    }

    public function delete(DeleteFileRequest $request, DeleteFileAction $action)
    {
        $fileIds = $request->get('files');
        $action->execute($fileIds);

        return new EmptyResource();
    }
}
