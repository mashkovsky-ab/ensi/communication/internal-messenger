<?php


namespace App\Http\ApiV1\Modules\Messages\Controllers;

use App\Domain\Messages\Actions\CreateChatAction;
use App\Domain\Messages\Actions\DeleteChatAction;
use App\Domain\Messages\Actions\PatchChatAction;
use App\Http\ApiV1\Modules\Messages\Queries\ChatsQuery;
use App\Http\ApiV1\Modules\Messages\Requests\CreateOrReplaceChatRequest;
use App\Http\ApiV1\Modules\Messages\Requests\PatchChatRequest;
use App\Http\ApiV1\Modules\Messages\Resources\ChatsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class ChatsController
{
    public function create(CreateOrReplaceChatRequest $request, CreateChatAction $action)
    {
        return new ChatsResource($action->execute($request->validated()));
    }

    public function patch(int $chatId, PatchChatRequest $request, PatchChatAction $action)
    {
        return new ChatsResource($action->execute($chatId, $request->validated()));
    }

    public function delete(int $chatId, DeleteChatAction $action)
    {
        $action->execute($chatId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, ChatsQuery $query)
    {
        return ChatsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
