<?php

namespace App\Http\ApiV1\Modules\Messages\Queries;

use App\Domain\Messages\Models\Chat;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ChatsQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Chat::query();

        parent::__construct($query);

        $this->allowedSorts(['id']);

        $this->allowedIncludes(['messages']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('user_id'),
            AllowedFilter::exact('type_id'),
            AllowedFilter::exact('unread_admin'),
            AllowedFilter::exact('user_type'),

            AllowedFilter::scope('theme_like'),
        ]);

        $this->defaultSort('id');
    }
}
