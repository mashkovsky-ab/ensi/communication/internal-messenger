<?php

namespace App\Http\ApiV1\Modules\Messages\Queries;

use App\Domain\Messages\Models\Message;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class MessagesQuery extends QueryBuilder
{
    public function __construct()
    {
        $query = Message::query();

        parent::__construct($query);

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('chat_id'),
            AllowedFilter::exact('user_id'),

            AllowedFilter::exact('chat.type_id'),
            AllowedFilter::exact('chat.unread_admin'),

            AllowedFilter::scope('chat.theme_like'),
            AllowedFilter::scope('created_at_from'),
            AllowedFilter::scope('created_at_to'),
        ]);

        $this->defaultSort('id');
    }
}
