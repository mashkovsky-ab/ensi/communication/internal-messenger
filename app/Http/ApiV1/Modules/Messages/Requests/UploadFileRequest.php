<?php

namespace App\Http\ApiV1\Modules\Messages\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class UploadFileRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => ['required', 'file', 'max:10240'],
            'name' => ['sometimes', 'required', 'string', 'max:150'],
        ];
    }
}
