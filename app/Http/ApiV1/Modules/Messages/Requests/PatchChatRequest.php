<?php

namespace App\Http\ApiV1\Modules\Messages\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\UserTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchChatRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'theme' => ['string'],
            'type_id' => ['integer'],
            'muted' => ['boolean'],
            'unread_user' => ['boolean'],
            'unread_admin' => ['boolean'],
            'user_type' => ['required', Rule::in(UserTypeEnum::cases())],
        ];
    }
}
