<?php

namespace App\Http\ApiV1\Modules\Messages\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\UserTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOrReplaceMessageRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'chat_id' => ['required', 'integer'],
            'user_id' => ['required', 'integer'],
            'text' => ['nullable', 'string'],
            'files' => ['nullable', 'array'],
            'user_type' => ['required', Rule::in(UserTypeEnum::cases())],
        ];
    }
}
