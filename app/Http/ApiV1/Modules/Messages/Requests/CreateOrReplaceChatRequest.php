<?php

namespace App\Http\ApiV1\Modules\Messages\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\ChatDirectionEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\UserTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOrReplaceChatRequest extends BaseFormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'direction' => ['required', Rule::in(ChatDirectionEnum::cases())],
            'theme' => ['required', 'string'],
            'type_id' => ['required', 'integer'],
            'muted' => ['required', 'boolean'],
            'user_id' => ['required', 'integer'],
            'unread_user' => ['nullable', 'boolean'],
            'unread_admin' => ['nullable', 'boolean'],
            'user_type' => ['required', Rule::in(UserTypeEnum::cases())],
        ];
    }
}
