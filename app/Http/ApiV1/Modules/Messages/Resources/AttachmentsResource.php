<?php

namespace App\Http\ApiV1\Modules\Messages\Resources;

use App\Domain\Messages\Models\Attachment;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;

/**
 * @mixin Attachment
 */
class AttachmentsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $fileManager = resolve(EnsiFilesystemManager::class);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'path' => $this->path,
            'disk' => $this->disk,
            'url' => $fileManager->public()->url($this->path),
        ];
    }
}
