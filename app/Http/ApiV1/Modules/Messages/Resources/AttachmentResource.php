<?php


namespace App\Http\ApiV1\Modules\Messages\Resources;


use App\Domain\Messages\Models\Attachment;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class AttachmentResource
 * @package App\Http\ApiV1\Modules\Messages\Resources
 *
 * @mixin Attachment
 */
class AttachmentResource extends BaseJsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'file' => $this->mapProtectedFileToResponse($this->path),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}