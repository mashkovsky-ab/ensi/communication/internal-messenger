<?php

namespace App\Http\ApiV1\Modules\Messages\Resources;

use App\Domain\Messages\Models\Message;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;

/** @mixin Message */
class MessagesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'chat_id' => $this->chat_id,
            'user_id' => $this->user_id,
            'user_type' => $this->user_type,
            'text' => $this->text,
            'files' => EnsiFile::collectionProtected($this->files ?: []),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
