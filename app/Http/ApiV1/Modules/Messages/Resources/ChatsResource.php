<?php

namespace App\Http\ApiV1\Modules\Messages\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin \App\Domain\Messages\Models\Chat */
class ChatsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'direction' => $this->direction,
            'theme' => $this->theme,
            'type_id' => $this->type_id,
            'muted' => $this->muted,
            'user_id' => $this->user_id,
            'user_type' => $this->user_type,
            'unread_user' => $this->unread_user,
            'unread_admin' => $this->unread_admin,
            'messages' => MessagesResource::collection($this->whenLoaded('messages')),
        ];
    }
}
