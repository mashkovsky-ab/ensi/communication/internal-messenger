<?php

namespace App\Domain\Example\Actions;

use Spatie\QueueableAction\QueueableAction;

class AsyncHandleKafkaTopicAction extends AbstractHandleKafkaTopicAction
{
    use QueueableAction;

    protected function getActionType(): string
    {
        return 'async';
    }
}
