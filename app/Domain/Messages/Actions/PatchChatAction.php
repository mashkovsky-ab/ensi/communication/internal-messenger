<?php

namespace App\Domain\Messages\Actions;

use App\Domain\Messages\Models\Chat;
use Illuminate\Support\Arr;

class PatchChatAction
{
    public function execute(int $chatId, array $fields): Chat
    {
        $chat = Chat::findOrFail($chatId);
        $chat->update($fields);

        return $chat;
    }
}
