<?php

namespace App\Domain\Messages\Actions;


use App\Domain\Messages\Models\Attachment;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Storage;

class UploadFileAction
{
    public function __construct(protected EnsiFilesystemManager $fileManager)
    {
    }

    public function execute(UploadedFile $file, $name = null): Attachment
    {
        $originalFileName = $name ?? $file->getClientOriginalName();
        $hash = Str::random(20);
        $extension = $file->getClientOriginalExtension() != '' ? $file->getClientOriginalExtension() : $file->extension();
        $fileName = "{$originalFileName}_{$hash}.{$extension}";

        $hashedSubDirs = $this->fileManager->getHashedDirsForFileName($fileName);

        $disk = Storage::disk($this->fileManager->protectedDiskName());

        $path = $disk->putFileAs("attachments/{$hashedSubDirs}", $file, $fileName);

        $attachment = new Attachment();
        $attachment->name = $fileName;
        $attachment->path = $path;
        $attachment->disk = 'protected';

        $attachment->save();

        return $attachment;
    }
}
