<?php

namespace App\Domain\Messages\Actions;

use App\Domain\Messages\Exceptions\MessageException;
use App\Domain\Messages\Models\Message;
use App\Domain\Messages\Models\Chat;

class CreateMessageAction
{
    /**
     * @throws MessageException
     */
    public function execute(int $chatId, string $text, int $userId, int $userType, array $files = []): Message
    {
        $chatExists = Chat::where('id', $chatId)->exists();

        if (!$chatExists) {
            $message = "An error occurred while saving the message: chat id=$chatId not found";
            throw new MessageException($message);
        }

        $message = new Message();
        $message->chat_id = $chatId;
        $message->text = $text;
        $message->user_id = $userId;
        $message->user_type = $userType;
        $message->files = $files;
        $message->save();

        return $message;
    }
}
