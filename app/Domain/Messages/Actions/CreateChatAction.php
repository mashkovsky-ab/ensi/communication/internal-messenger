<?php

namespace App\Domain\Messages\Actions;

use App\Domain\Messages\Models\Chat;
use Illuminate\Support\Arr;

class CreateChatAction
{
    public function execute(array $fields): Chat
    {
        return Chat::create(Arr::only($fields, Chat::FILLABLE));
    }
}
