<?php

namespace App\Domain\Messages\Actions\Kafka;

use App\Domain\Messages\Exceptions\MessageException;
use App\Domain\Messages\Actions\CreateMessageAction;
use Ensi\InternalMessenger\Dto\MessageForCreate;
use Ensi\InternalMessenger\ObjectSerializer;
use App\Domain\Messages\Models\Message;
use Ensi\InternalMessenger\Dto\File;
use RdKafka\Message as KafkaMessage;
use Throwable;

class SyncCreateMessageAction
{
    /**
     * @throws MessageException
     */
    public function execute(KafkaMessage $kafkaMessage): Message
    {
        try {
            /** @var MessageForCreate $message */
            $message = ObjectSerializer::deserialize($kafkaMessage->payload, MessageForCreate::class);

            $files = array_map(function (File $file) {
                return [
                    'path' => $file->getPath(),
                    'root_path' => $file->getRootPath(),
                    'url' => $file->getUrl()
                ];
            }, $message->getFiles() ?? []);

            return (new CreateMessageAction())->execute(
                $message->getChatId(),
                $message->getText(),
                $message->getUserId(),
                $message->getUserType(),
                $files
            );
        } catch (Throwable $e) {
            $message = 'An error occurred while saving the message,'
                . " kafka message payload: {$kafkaMessage->payload}\n"
                . $e->getMessage();
            throw new MessageException($message);
        }
    }
}
