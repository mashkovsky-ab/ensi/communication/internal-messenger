<?php

namespace App\Domain\Messages\Actions;


use App\Domain\Messages\Models\Attachment;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;

class DeleteFileAction
{
    public function __construct(protected EnsiFilesystemManager $fileManager)
    {
    }

    public function execute(array $fileIds)
    {
        $attachments = Attachment::query()->whereIn('id', $fileIds)->get();
        $paths = $attachments->pluck('path')->toArray();
        $this->fileManager->delete($paths);
        foreach ($attachments as $attachment) {
            $attachment->delete();
        }
    }
}
