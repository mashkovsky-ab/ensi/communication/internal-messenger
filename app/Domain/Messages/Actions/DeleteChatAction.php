<?php

namespace App\Domain\Messages\Actions;

use App\Domain\Messages\Models\Chat;

class DeleteChatAction
{
    public function execute(int $chatId): void
    {
        Chat::destroy($chatId);
    }
}
