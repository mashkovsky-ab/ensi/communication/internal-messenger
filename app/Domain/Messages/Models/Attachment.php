<?php

namespace App\Domain\Messages\Models;


use Carbon\Carbon;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $path
 * @property string $disk
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Attachment extends Model
{
    protected static function boot()
    {
        parent::boot();

        self::deleted(function (self $attachment) {
            if ($attachment->path) {
                $filesystem = resolve(EnsiFilesystemManager::class);
                $disk = $attachment->disk === 'protected' ? $filesystem->protected() : $filesystem->public();
                $disk->delete($attachment->path);
            }
        });
    }
}
