<?php

namespace App\Domain\Messages\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Message
 * @package App\Domain\Messages\Models
 *
 * @property int $id
 * @property int $chat_id
 * @property int $user_id
 * @property string $text
 * @property array $files
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $user_type
 *
 * @property-read Chat $chat
 */
class Message extends Model
{
    protected $table = 'messages';

    const FILLABLE = [
        'chat_id', 'user_id', 'text', 'files', 'user_type',
    ];

    protected $casts = [
        'files' => 'array',
    ];

    protected $fillable = self::FILLABLE;

    public function chat(): BelongsTo
    {
        return $this->belongsTo(Chat::class, 'chat_id', 'id');
    }

    public function scopeCreatedAtFrom(Builder $query, $date): Builder
    {
        return $query->where('created_at', '>=', $date);
    }

    public function scopeCreatedAtTo(Builder $query, $date): Builder
    {
        return $query->where('created_at', '<=', $date);
    }
}
