<?php

namespace App\Domain\Messages\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Chat
 * @package App\Domain\Messages\Models
 *
 * @property int $id
 * @property int $direction
 * @property string|null $theme
 * @property integer|null $type_id
 * @property boolean $muted
 * @property int $user_id
 * @property boolean $unread_user
 * @property boolean $unread_admin
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $user_type
 *
 * @property-read Collection|Message[] $messages
 */
class Chat extends Model
{
    const DIRECTION_IN = 1; // Входящий (инициировал клиент)
    const DIRECTION_OUT = 2; // Исходящий (инициировал админ)

    protected $table = 'chats';

    const FILLABLE = [
        'direction', 'theme', 'type_id', 'muted', 'user_id', 'unread_user', 'unread_admin', 'user_type'
    ];

    /**
     * @var array
     */
    protected $fillable = self::FILLABLE;

    public function messages(): HasMany
    {
        return $this->hasMany(Message::class, 'chat_id', 'id');
    }

    public function scopeThemeLike(Builder $query, $theme): Builder
    {
        return $query->where('theme', 'ilike', "%{$theme}%");
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($chat) {
            foreach ($chat->messages as $message) {
                $message->delete();
            }
        });
    }
}
