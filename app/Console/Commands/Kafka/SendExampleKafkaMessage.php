<?php

namespace App\Console\Commands\Kafka;

use Ensi\LaravelPhpRdKafkaProducer\HighLevelProducer;
use Ensi\InternalMessenger\Dto\File;
use Illuminate\Console\Command;
use Throwable;

class SendExampleKafkaMessage extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'kafka:send-example-message';

    /**
     * The console command description.
     */
    protected $description = 'Send a message with default data to'
    . ' <contour>.communication.test.example.1 kafka topic'
    . ' (hint: check your config cache if processor for topic exists and not found)';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $file = new File();
        $file->setUrl('url');
        $file->setPath('path');
        $file->setRootPath('root-path');

        try {
            $topic = topic('communication.test.example.1');
            (new HighLevelProducer($topic))->sendOne($file->__toString());
        } catch (Throwable $e) {
            $this->error('An error occurred while sending a message to kafka');
            $this->line($e->getMessage());

            return 1;
        }

        $this->line('The message was sent successfully');

        return 0;
    }
}
