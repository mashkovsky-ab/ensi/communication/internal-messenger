<?php

if (!function_exists('topic')) {
    function topic(string $name): string
    {
        return config('kafka.contour') . ".$name";
    }
}
