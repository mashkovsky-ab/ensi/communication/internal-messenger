<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('direction');
            $table->string('theme')->nullable();
            $table->integer('type')->nullable();
            $table->boolean('muted')->default(false);
            $table->bigInteger('user_id');
            $table->boolean('unread_user')->default(false);
            $table->boolean('unread_admin')->default(false);
            $table->integer('user_type');

            $table->timestamps();
        });

        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('chat_id');
            $table->bigInteger('user_id');
            $table->text('text');
            $table->integer('user_type');

            $table->timestamps();
            $table->foreign('chat_id')->references('id')->on('chats');
        });

        Schema::create('attachments', function (Blueprint $table) {
            $table->id();
            $table->integer('message_id');
            $table->string('name');
            $table->string('path');
            $table->string('url');
            $table->timestamps();
            $table->foreign('message_id')->references('id')->on('messages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
        Schema::dropIfExists('messages');
        Schema::dropIfExists('chats');
    }
};
